# Trivia-game

VueJS trivia game.

Using [Open Trivia DB API](https://opentdb.com/api_config.php), this game generates trivia questions based on the selected category. The player will have to answer 10 questions before being presented with the correct answers and final score.

## Run project locally
To run the project locally make sure to install npm dependencies first with:

```
npm install
```

And then run the project with:

```
npm run serve
```

## Deployment

The game has been deployed to Heroku: https://triviagame-app.herokuapp.com/ \
`Express JS` is used to serve the application.

To build the app for deployment:

```
npm run build
```

The app can then be pushed to a Heroku repository.

The script `"start": "node server.js"` in `package.json` will make Heroku serve the app with Express JS.

## Authors
* [Ahmad Abdulal](https://gitlab.com/ahab-lab)
* [Nuno Cunha](https://gitlab.com/iamnuno/)
