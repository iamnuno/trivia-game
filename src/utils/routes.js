import start from "../components/Start.vue";
import categories from "../components/Categories.vue";
import Quiz from "../components/Quiz.vue";
import Error from "../components/Error.vue";

export default [
    { path: "/start", name: "start", component: start },
    { path: "/", name: "start", component: start },
    { path: "/categories", name: "categories", component: categories },
    { path: "/quiz/:category", name: "quiz", component: Quiz },
    { path: "/error", name: "error", component: Error },
]; 