const TRIVIA_API_base_URL = "https://opentdb.com/api.php?";
const questionsAmountURL = "amount=";
const categoryURl = "&category=";

export async function fetchQuestions(category) {
    try {
        const response = await fetch(
            TRIVIA_API_base_URL +
                questionsAmountURL +
                10 +
                categoryURl +
                category
        );
        const payload = await response.json();
        return payload.results;
    } catch (err) {
        return null;
    }
}

export const categories = {
    "General Knowledge": 9,
    Books: 10,
    Films: 11,
    Music: 12,
    "Musicals and Theaters": 13,
    Television: 14,
    "Video Games": 15,
    "Board Games": 16,
    "Science & Nature": 17,
    Computers: 18,
    Mathematics: 19,
    Mythology: 20,
    Sports: 21,
    Geography: 22,
    History: 23,
    Politics: 24,
    Art: 25,
    Celebrities: 26,
    Animals: 27,
    Vehicles: 28,
    Comics: 29,
    Gadgets: 30,
    "Japanese Anime & Manga": 31,
    "Cartoon & Animation": 32,
};
