import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './utils/routes.js'
import { store } from './utils/store'

Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router: router,
  store: store
}).$mount('#app')